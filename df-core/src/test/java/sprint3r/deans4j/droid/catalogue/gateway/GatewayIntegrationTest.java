package sprint3r.deans4j.droid.catalogue.gateway;

import java.io.IOException;

import javax.inject.Inject;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;

import com.google.mockwebserver.MockResponse;
import com.google.mockwebserver.MockWebServer;

import dagger.ObjectGraph;

public class GatewayIntegrationTest {
	
	@Inject
	DFProductGateway gateway;
	
	MockWebServer mockWebServer = new MockWebServer();
	
	@Before
	public void setUp() throws IOException {
		mockWebServer.enqueue(new MockResponse().setBody("{\"metaData\":{\"totalSize\":1,\"currentOffset\":0,\"currentLimit\":0},\"items\":[{\"id\":1,\"name\":\"product 1\",\"description\":\"product description 1\",\"postedDate\":\"2014-02-17 10:23\",\"latestUpdate\":\"2014-02-17 10:23\",\"price\":30.0,\"property\":null,\"images\":[],\"poster\":{\"userId\":\"manee\",\"address\":{\"line1\":\"sukumvit road\",\"line2\":\"bangna\",\"province\":\"bangkok\",\"country\":\"Thailand\"},\"contact\":{\"email\":\"manee@dealfish.com\",\"mobile\":\"08931231234\"}},\"categoryId\":1}]}"));
		mockWebServer.play();
		
		ObjectGraph.create(new MockWebServerModule(mockWebServer)).inject(this);
	}

	@Test @Ignore
	public void parsingAndNetworkOK() throws IOException {
		Assertions.assertThat(gateway.find(1).getName()).isEqualTo("product 1");
	}
	
}
