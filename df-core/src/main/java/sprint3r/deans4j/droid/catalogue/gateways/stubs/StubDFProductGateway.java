package sprint3r.deans4j.droid.catalogue.gateways.stubs;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.http.Body;
import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.DFUser;
import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchMetaData;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;


@Singleton
public class StubDFProductGateway implements DFProductGateway {

	@Inject
	ModelHolders holders;

	@Override
	public Product post(@Body PostProduct postProduct) {

		DFUser user = findUserById(postProduct.getUserId());

		Product product = postProduct.getProduct();
		product.setPoster(user);
		product.setId(holders.getProducts().size() + 1);
		holders.getProducts().add(product);

		return product;
	}

	@Override
	public Product find(Integer productId) {
		List<Product> products = holders.getProducts();
		return filterById(productId, products);
	}

	@Override
	public SearchResult<Product> listProductByCategory(Integer categoryId,
			Integer offset, Integer limit) {

		List<Product> products = holders.getProducts();
		List<Product> filterByCategory = filterByCategory(categoryId, products);
		List<Product> filteredBySize = filterBySize(filterByCategory, offset,
				limit);

		return new SearchResult<Product>(
				filteredBySize.toArray(new Product[filteredBySize.size()]),
				new SearchMetaData(filterByCategory.size(), offset,
						filteredBySize.size() - 1));
	}

	@Override
	public SearchResult<Product> listProductByUser(String userId,
			Integer offset, Integer limit) {
		List<Product> products = holders.getProducts();
		List<Product> filterByUsername = filterProductByUserId(userId, products);
		List<Product> filteredBySize = filterBySize(filterByUsername, offset,
				limit);

		return new SearchResult<Product>(
				filteredBySize.toArray(new Product[filteredBySize.size()]),
				new SearchMetaData(filterByUsername.size(), offset,
						filteredBySize.size() - 1));
	}

	@Override
	public SearchResult<Product> listFavorites(String userId, int offset,
			int limit) {
		return new SearchResult<Product>(new Product[0], new SearchMetaData(0,
				offset, limit));
	}

	private Product filterById(Integer productId, List<Product> products) {
		for (Product p : products) {
			if (p.getId().equals(productId)) {
				return p;
			}
		}

		throw new RuntimeException("no match");
	}

	private List<Product> filterByCategory(Integer categoryId,
			List<Product> products) {

		List<Product> filterByCategory = new ArrayList<Product>();
		for (Product p : products) {
			if (p.getCategoryId().equals(categoryId)) {
				filterByCategory.add(p);
			}
		}

		return filterByCategory;
	}

	private List<Product> filterProductByUserId(String userId,
			List<Product> products) {

		List<Product> filterByUsername = new ArrayList<Product>();
		for (Product p : products) {
			if (p.getPoster().getUserId().equals(userId)) {
				filterByUsername.add(p);
			}
		}

		return filterByUsername;
	}

	private List<Product> filterBySize(List<Product> products, Integer offset,
			Integer limit) {
		int index = offset < products.size() ? offset : products.size();
		int size = (limit + index < products.size()) ? limit + index : products
				.size();

		return products.subList(index, size);
	}

	private DFUser findUserById(String userId) {
		for (DFUser user : holders.getUsers()) {
			if (userId.equals(user.getUserId())) {
				return user;
			}
		}

		throw new RuntimeException("no match");
	}

}
