package sprint3r.deans4j.droid.catalogue.modules;

import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import com.fasterxml.jackson.databind.ObjectMapper;

import dagger.Module;
import dagger.Provides;

@Module(library = true)
public class JacksonModule {
	
	@Provides @Singleton
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm"));
		return mapper;
	}
}
