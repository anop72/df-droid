package sprint3r.deans4j.droid.catalogue.gateways.observable;

import retrofit.client.Response;
import rx.Observable;


public interface ObservableDFUserFavoriteGateway {
	
	Observable<Response> addToFavorites(String userId, String productId);
	
	Observable<Response> removeFromFavorites(String userId, String productId);

}
