package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PostProduct implements Serializable {

	private static final long serialVersionUID = -4802335895146271613L;

	String userId;
	Product product;
	
	public PostProduct() {
		
	}

	public PostProduct(String userId, Product product) {
		this.userId = userId;
		this.product = product;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
