package sprint3r.deans4j.droid.catalogue.application;

import sprint3r.deans4j.droid.catalogue.models.PostProduct;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface AppUserFacade {
	
	void postNewProduct(PostProduct postProduct);
	
	void bookmarkProduct(Integer productId);
	
	SearchResult<Product> viewBookmarkProducts();
	
}
