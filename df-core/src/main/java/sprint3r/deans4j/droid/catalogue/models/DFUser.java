package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DFUser implements Serializable {
	
	private static final long serialVersionUID = -1000185716181973019L;
	
	String userId;
	Address address;
	Contact contact;
	
	public DFUser() {
	}
	
	public DFUser(String userId, Address address, Contact contact) {
		this.userId =  userId;
		this.address = address;
		this.contact = contact;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	
}
