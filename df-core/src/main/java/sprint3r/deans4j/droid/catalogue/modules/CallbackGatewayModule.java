package sprint3r.deans4j.droid.catalogue.modules;

import javax.inject.Singleton;

import retrofit.RestAdapter;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import dagger.Module;
import dagger.Provides;

@Module(library = true, includes=RetrofitModule.class, complete = false)
public class CallbackGatewayModule {

	@Provides @Singleton
	public CallbackDFProductGateway callbackProductGateway(RestAdapter restAdapter) {
		return restAdapter.create(CallbackDFProductGateway.class);
	}
	
	@Provides @Singleton
	public CallbackDFCategoryGateway callbackCategoryGateway(RestAdapter restAdapter) {
		return restAdapter.create(CallbackDFCategoryGateway.class);
	}
}
