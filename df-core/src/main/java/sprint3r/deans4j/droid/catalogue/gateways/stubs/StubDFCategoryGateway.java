package sprint3r.deans4j.droid.catalogue.gateways.stubs;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.http.GET;
import sprint3r.deans4j.droid.catalogue.gateways.DFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.SearchMetaData;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

@Singleton
public class StubDFCategoryGateway implements DFCategoryGateway {

	@Inject
	ModelHolders holders;
	
	@Override
	@GET("/categories")
	public SearchResult<Category> listAllCategories() {
		List<Category> categories = holders.getCategories();
		return new SearchResult<Category>(categories.toArray(new Category[categories.size()]), new SearchMetaData(categories.size(), 0, categories.size() - 1));
	}
	
}
