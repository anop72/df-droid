package sprint3r.deans4j.droid.catalogue.gateways;

import retrofit.http.GET;
import sprint3r.deans4j.droid.catalogue.models.Category;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

public interface DFCategoryGateway {
	
	@GET("/categories")
	SearchResult<Category> listAllCategories();

}
