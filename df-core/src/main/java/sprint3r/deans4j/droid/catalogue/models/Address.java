package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements Serializable {
	
	private static final long serialVersionUID = 4169640402865370613L;
	
	String line1;
	String line2;
	String province;
	String country;
	
	public Address() {
	}
	
	public Address(String line1, String line2, String province, String country) {
		this.line1 = line1;
		this.line2 = line2;
		this.province = province;
		this.country = country;
	}
	
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
