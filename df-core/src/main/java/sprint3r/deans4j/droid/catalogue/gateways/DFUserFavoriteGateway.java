package sprint3r.deans4j.droid.catalogue.gateways;

import retrofit.client.Response;

public interface DFUserFavoriteGateway {
	
	Response addToFavorites(String userId, String productId);
	
	Response removeFromFavorites(String userId, String productId);

}
