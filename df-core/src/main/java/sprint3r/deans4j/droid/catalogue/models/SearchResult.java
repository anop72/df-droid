package sprint3r.deans4j.droid.catalogue.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult<T> implements Serializable {
	
	private static final long serialVersionUID = 5061934174890913163L;
	
	SearchMetaData metaData;
	T[] items;
	
	public SearchResult() {
		
	}
	
	public SearchResult(T[] items, SearchMetaData meta) {
		this.items = items;
		this.metaData = meta;
	}
	public SearchMetaData getMetaData() {
		return metaData;
	}
	public void setMetaData(SearchMetaData metaData) {
		this.metaData = metaData;
	}
	public T[] getItems() {
		return items;
	}
	public void setItems(T[] items) {
		this.items = items;
	}

}
