package sprint3r.deans4j.droid.catalogue.modules;

import javax.inject.Singleton;

import retrofit.RestAdapter;
import sprint3r.deans4j.droid.catalogue.gateways.observable.ObservableDFCategoryGateway;
import sprint3r.deans4j.droid.catalogue.gateways.observable.ObservableDFProductGateway;
import dagger.Module;
import dagger.Provides;

@Module(library = true, includes=RetrofitModule.class, complete = false)
public class ObservableGatewayModule {
	
	@Provides @Singleton
	public ObservableDFProductGateway observableProductGateway(RestAdapter restAdapter) {
		return restAdapter.create(ObservableDFProductGateway.class);
	}
	
	@Provides @Singleton
	public ObservableDFCategoryGateway observableCategoryGateway(RestAdapter restAdapter) {
		return restAdapter.create(ObservableDFCategoryGateway.class);
	}
}
