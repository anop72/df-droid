
package com.dealfish.app.modules;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import retrofit.ChangeableServer;
import retrofit.Server;
import sprint3r.deans4j.droid.catalogue.gateways.DFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import sprint3r.deans4j.droid.catalogue.modules.CallbackGatewayModule;
import sprint3r.deans4j.droid.catalogue.modules.ObservableGatewayModule;
import sprint3r.deans4j.droid.catalogue.modules.SyncGatewayModule;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.dealfish.app.domains.callbacks.OttoBasicCallback;
import com.dealfish.app.domains.tape.TapeModule;
import com.dealfish.app.droid.AndroidBaseModule;
import com.dealfish.app.droid.ForDFApplication;
import com.dealfish.app.lesson1.retrofit._1asynctaskloader.LoaderProductListFragment;
import com.dealfish.app.lesson1.retrofit._1asynctaskloader.LoaderProductsInCategoryActivity;
import com.dealfish.app.lesson1.retrofit._1asynctaskloader.ProductsAsyncTaskLoader;
import com.dealfish.app.lesson1.retrofit._2callback.CallbackProductListFragment;
import com.dealfish.app.lesson1.retrofit._2callback.CallbackProductsInCategoryActivity;
import com.dealfish.app.lesson1.retrofit._3rxjava.RxJavaProductListFragment;
import com.dealfish.app.lesson1.retrofit._3rxjava.RxJavaProductsInCategoryActivity;
import com.dealfish.app.lesson2.ottobase._1selfcallback.OttoProductsWithSelfCallbackActivity;
import com.dealfish.app.lesson2.ottobase._2externalcallback.OttoProductsWithExternalCallbackActivity;
import com.dealfish.app.lesson2.ottobase._3externalloader.OttoProductsWithLoaderActivity;
import com.dealfish.app.lesson3.tape.droid.PostNewProductActivity;
import com.dealfish.app.lesson3.tape.droid.PostNewProductProcessorService;
import com.dealfish.app.lesson4.complete.droid.AdvancedCategoryActivity;
import com.dealfish.app.lesson4.complete.droid.AdvancedOttoProductListFragment;
import com.dealfish.app.lesson4.complete.droid.AdvancedProductActivity;
import com.dealfish.app.lesson4.complete.droid.OttoCategoryListFragment;
import com.dealfish.app.lesson4.complete.droid.OttoProductDetailFragment;
import com.dealfish.app.lesson4.complete.droid.OttoProductListFragment;
import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;

@Module(includes = { 
			AndroidBaseModule.class,
			SyncGatewayModule.class, 
			ObservableGatewayModule.class,
			CallbackGatewayModule.class,
			TapeModule.class
		}, 
		complete = true, 
		injects = {
			LoaderProductsInCategoryActivity.class, LoaderProductListFragment.class,
			RxJavaProductsInCategoryActivity.class, RxJavaProductListFragment.class,
			CallbackProductsInCategoryActivity.class, CallbackProductListFragment.class,
			OttoProductDetailFragment.class, OttoProductListFragment.class,
			OttoProductsWithSelfCallbackActivity.class, OttoProductsWithExternalCallbackActivity.class, OttoProductsWithLoaderActivity.class,
			PostNewProductActivity.class,
			PostNewProductProcessorService.class, AdvancedCategoryActivity.class, AdvancedProductActivity.class,
			OttoCategoryListFragment.class, AdvancedOttoProductListFragment.class,
		})
public class AppModule {
	
	@Provides @Singleton
	public Server changeableEndpoint() {
		return new ChangeableServer("http://54.213.123.110:8080/df-server", "test");
	}
	
	@Provides
	ProductsAsyncTaskLoader productLoader(@ForDFApplication Context context, DFProductGateway gateway) {
		return new ProductsAsyncTaskLoader(context, gateway);
	}

	@Provides @Singleton @Named("productListOtto")
	public Fragment provideProductListFragment() {
		return new OttoProductListFragment();
	}
	
	@Provides @Singleton @Named("productDetailOtto")
	public Fragment provideProductDetailFragment() {
		return new OttoProductDetailFragment();
	}
	
	@Provides
	public OttoBasicCallback<SearchResult<Product>> provideBasicCallback(Bus bus) {
		return new OttoBasicCallback<SearchResult<Product>>(bus);
	}
	
	@Provides @Singleton @Named("cachedFile")
	public File cacheFile(@ForDFApplication Context context) {
		return new File(context.getFilesDir(), "okhttp-cache");
	}
	
	@Provides @Singleton @Named("queueFile")
	public File queueFile(@ForDFApplication Context context) {
		return new File(context.getFilesDir(), "queue-file");
	}
}
