package com.dealfish.app.lesson4.complete.droid;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import sprint3r.deans4j.droid.catalogue.models.Product;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.dealfish.app.domains.ProductList;
import com.dealfish.app.domains.ProductList.OnDataChangedEvent;
import com.dealfish.app.domains.SomethingBadHappendEvent;
import com.dealfish.app.droid.DFBaseListFragment;
import com.dealfish.app.lesson4.complete.droid.EndlessScrollListener.ScrollToEndEvent;
import com.squareup.otto.Subscribe;

public class AdvancedOttoProductListFragment extends DFBaseListFragment {

	@Inject
	EndlessScrollListener endlessScrollListener;
	
	private ProductList productList;
	private List<String> productNames = new ArrayList<String>();
	private ArrayAdapter<String> adapter;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		
		setEmptyText("Please wait while loading ...");
		
		adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, productNames);
		setListAdapter(adapter);
		getListView().setOnScrollListener(endlessScrollListener);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		postSelectedItem(null);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		postSelectedItem(productList.getProducts().get(position));
	}
	
	@Subscribe
	public void onProductList(ProductList productList) {
		this.productList = productList;
		if (this.productList.getProducts().isEmpty()) {
			this.productList.loadMore();
		}
	}

	@Subscribe
	public void onDataChanged(OnDataChangedEvent event) {
		updateListAdapter(event.products);
	}
	
	@Subscribe
	public void onScrolledToBottom(ScrollToEndEvent event) {
		productList.loadMore();
	}
	
	@Subscribe
	public void onLoadingError(SomethingBadHappendEvent e) {
		productNames.clear();
		adapter.notifyDataSetChanged();
		Toast.makeText(getActivity(), "something bad happen: " + e.getThrowable().getMessage(), Toast.LENGTH_SHORT).show();
	}
	
	private void postSelectedItem(Product product) {
		getBus().post(new AdvancedProductActivity.SelectedProductItemEvent(product));
	}
	
	private void updateListAdapter(Iterable<Product> data) {
		productNames.clear();
		for (Product d : data) {
			productNames.add(d.getName());
		}
		adapter.notifyDataSetChanged();
	}

}