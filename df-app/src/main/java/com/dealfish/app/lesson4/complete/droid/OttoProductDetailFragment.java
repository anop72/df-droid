package com.dealfish.app.lesson4.complete.droid;

import sprint3r.deans4j.droid.catalogue.models.Product;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dealfish.app.R;
import com.dealfish.app.droid.DFBaseFragment;
import com.squareup.otto.Subscribe;

public class OttoProductDetailFragment extends DFBaseFragment {

	private Product selectedProduct;
	private TextView productName;
	private TextView productPrice;
	private TextView productDescription;
	private TextView productPoster;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_product_detail, container, false);
		
		productName = (TextView) rootView.findViewById(R.id.product_name);
		productPrice = (TextView) rootView.findViewById(R.id.product_price);
		productDescription = (TextView) rootView.findViewById(R.id.product_description);
		productPoster = (TextView) rootView.findViewById(R.id.product_poster);
		
		return rootView;
	}
	
	@Subscribe
	public void setViewProduct(Product product) {
		this.selectedProduct = product;
		renderProduct(selectedProduct);
		this.getActivity().setTitle(selectedProduct.getName());
	}

	private void renderProduct(Product product) {
		String userId = (product.getPoster() != null) ? product.getPoster().getUserId() : "unknown";
		this.productName.setText("Name: " + product.getName());
		this.productPrice.setText("Price: " + product.getPrice());
		this.productDescription.setText("Description: " + product.getDescription());
		this.productPoster.setText("Poster: " + userId);
	}
	
	public Product getSelectedProduct() {
		return selectedProduct;
	}
}
