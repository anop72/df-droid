package com.dealfish.app.lesson2.ottobase._1selfcallback;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

import com.dealfish.app.R;
import com.dealfish.app.droid.DFBaseActivity;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

public class OttoProductsWithSelfCallbackActivity extends DFBaseActivity implements Callback<SearchResult<Product>> {
	
	@Inject
	CallbackDFProductGateway gateway;
	
	@Inject  @Named("productListOtto")
	Fragment listWidget;
	
	@Inject @Named("productDetailOtto")
	Fragment detailWidget;
	
	private Product selectedProductItem;

	private Product[] products;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		renderProductListWidget();
		loadProducts();
	}

	private void loadProducts() {
		gateway.listProductByCategory(1, 0, 100, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void renderProductListWidget() {
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, listWidget);
		transaction.commit();
	}
	
	public void renderProductDetailWidget() {
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(android.R.id.content, detailWidget);
		transaction.addToBackStack(null);
		transaction.commit();
	}
	
	@Subscribe
	public void viewSelectedProduct(Product product) {
		this.selectedProductItem = product;
		renderProductDetailWidget();
	}

	@Produce
	public Product getSelectedProductItem() {
		return selectedProductItem;
	}
	
	@Produce
	public Product[] getProductList() {
		return this.products;
	}
	
	@Override
	public void success(SearchResult<Product> result, Response response) {
		this.products = result.getItems();
		getBus().post(this.products);
	}

	@Override
	public void failure(RetrofitError error) {
		
	}

}
