package com.dealfish.app.lesson1.retrofit._1asynctaskloader;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import com.dealfish.app.R;
import com.dealfish.app.droid.DFBaseActivity;

public class LoaderProductsInCategoryActivity extends DFBaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FragmentManager fm = getSupportFragmentManager();
		if (fm.findFragmentById(android.R.id.content) == null) {
			LoaderProductListFragment list = new LoaderProductListFragment();
			fm.beginTransaction().add(android.R.id.content, list).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
