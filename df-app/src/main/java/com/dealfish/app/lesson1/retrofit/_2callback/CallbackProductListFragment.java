package com.dealfish.app.lesson1.retrofit._2callback;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;
import sprint3r.deans4j.droid.catalogue.models.Product;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dealfish.app.droid.DFBaseListFragment;

public class CallbackProductListFragment extends DFBaseListFragment implements Callback<SearchResult<Product>> {
	
	@Inject
	CallbackDFProductGateway gateway;

	private ArrayAdapter<String> mAdapter;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);

		setEmptyText("No products");
		setListAdapter(mAdapter);
		setListShown(false);
		
		gateway.listProductByCategory(1, 0, 100, this);

	}

	@Override
	public void success(SearchResult<Product> t, Response response) {
		
		Product[] data = t.getItems();
		
		mAdapter.clear();

		for (Product d : data) {
			mAdapter.add(d.getName());
		}

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	@Override
	public void failure(RetrofitError error) {
		mAdapter.clear();
		setListShown(true);
		Toast.makeText(getActivity(), "something bad happen: " + error.getMessage(), Toast.LENGTH_SHORT).show();
	}


}