package com.dealfish.app.domains.tape;

import javax.inject.Inject;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFProductGateway;

import com.squareup.tape.TaskInjector;

@Singleton
public class NewPostTaskItemDependencyInjector implements TaskInjector<NewPostTaskItem> {
	
	private CallbackDFProductGateway gateway;

	@Inject
	public NewPostTaskItemDependencyInjector(CallbackDFProductGateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public void injectMembers(NewPostTaskItem task) {
		task.setGateway(this.gateway);
	}

}
