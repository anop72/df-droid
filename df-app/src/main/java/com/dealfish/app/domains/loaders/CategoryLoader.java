package com.dealfish.app.domains.loaders;

import javax.inject.Inject;
import javax.inject.Singleton;

import sprint3r.deans4j.droid.catalogue.gateways.callbacks.CallbackDFCategoryGateway;

import com.dealfish.app.domains.callbacks.OttoCategoryCache;

@Singleton
public class CategoryLoader {

	private CallbackDFCategoryGateway gateway;
	private OttoCategoryCache cacheCallback;

	@Inject
	public CategoryLoader(CallbackDFCategoryGateway gateway, OttoCategoryCache cacheCallback) {
		this.gateway = gateway;
		this.cacheCallback = cacheCallback;
	}

	public void requestListOfCategories() {
		if (cacheCallback.isCached()) {
			cacheCallback.postCacheValue();
		} else {
			this.gateway.listAllCategories(cacheCallback);
		}
	}
	
}
