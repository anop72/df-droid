package com.dealfish.app.domains.callbacks;

import java.lang.ref.WeakReference;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

import com.dealfish.app.domains.SomethingBadHappendEvent;
import com.squareup.otto.Bus;

public class OttoCacheCallback<T> implements Callback<SearchResult<T>> {
	
	private Bus bus;
	
	private WeakReference<T[]> items = new WeakReference<T[]>(null);
	
	public OttoCacheCallback(Bus bus) {
		this.bus = bus;
	}

	@Override
	public void success(SearchResult<T> result, Response response) {
		cacheValue(result);
		postCacheValue();
	}

	@Override
	public void failure(RetrofitError error) {
		postError(error);
	}

	public T[] getCachedValue() {
		return items.get();
	}
	
	public boolean isCached() {
		return getCachedValue() != null;
	}
	
	public void clearCache() {
		this.items.clear();
	}
	
	public void postCacheValue() {
		if (isCached()) bus.post(getCachedValue());
	}
	
	public void postError(RetrofitError error) {
		bus.post(new SomethingBadHappendEvent(error));
	}

	private void cacheValue(SearchResult<T> result) {
		this.items = new WeakReference<T[]>(result.getItems());
	}

}