package com.dealfish.app.domains.tape;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.tape.ObjectQueue;
import com.squareup.tape.TaskInjector;
import com.squareup.tape.TaskQueue;

public class NewPostTaskQueue extends TaskQueue<NewPostTaskItem> {

	private Bus bus;

	public NewPostTaskQueue(ObjectQueue<NewPostTaskItem> delegate, TaskInjector<NewPostTaskItem> taskInjector, Bus bus) {
		super(delegate, taskInjector);
		this.bus = bus;
		bus.register(this);
	}

	@Override
	public void add(NewPostTaskItem entry) {
		super.add(entry);
		bus.post(queueSizeChanged());
	}

	@Override
	public void remove() {
		super.remove();
		bus.post(queueSizeChanged());
	}

	@Produce
	public PostNewProductQueueSizeEvent queueSizeChanged() {
		return new PostNewProductQueueSizeEvent(size());
	}

	public static class PostNewProductQueueSizeEvent {

		private int size;

		public PostNewProductQueueSizeEvent(int size) {
			this.size = size;
		}

		public int getSize() {
			return size;
		}
	}

}