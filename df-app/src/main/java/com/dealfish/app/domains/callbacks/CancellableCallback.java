package com.dealfish.app.domains.callbacks;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CancellableCallback<T> implements Callback<T> {

	private boolean isCancelled = false;
	private boolean isLoading = true;

	private Callback<T> delegate;

	public CancellableCallback(Callback<T> delegate) {
		this.delegate = delegate;
	}

	@Override
	public void success(T t, Response response) {
		isLoading = false;
		if (isCancelled())
			return;

		delegate.success(t, response);
	}

	@Override
	public void failure(RetrofitError error) {
		isLoading = false;
		if (isCancelled())
			return;
		
		delegate.failure(error);
	}

	public void cancel() {
		
		if (isLoading()) {
			this.isCancelled = true;
			this.isLoading = false;
		}
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public boolean isLoading() {
		return isLoading;
	}
}
