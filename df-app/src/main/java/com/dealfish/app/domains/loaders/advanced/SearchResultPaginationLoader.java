package com.dealfish.app.domains.loaders.advanced;


import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprint3r.deans4j.droid.catalogue.models.SearchResult;

import com.dealfish.app.domains.callbacks.CallbackCallback;
import com.dealfish.app.domains.callbacks.CancellableCallback;
import com.squareup.otto.Bus;

public class SearchResultPaginationLoader<T> {
	
	private final Callback<SearchResult<T>> callback;
	private final DoRequestHandler<SearchResult<T>> doRequestHandler;
	private final Bus bus;
	
	private CancellableCallback<SearchResult<T>> recentCall;
	private int nextOffset = 0;
	private int maxOffset = Integer.MAX_VALUE;
	private static final int LIMIT = 20;

	public SearchResultPaginationLoader(DoRequestHandler<SearchResult<T>> doRequestHandler, Callback<SearchResult<T>> callback, Bus bus) {
		this.doRequestHandler = doRequestHandler;
		this.callback = callback;
		this.bus = bus;
	}

	public void loadNext() {
		if (hasMore() == false || recentRequestIsStillLoading()) return;
		makeRequest();
	}

	public boolean hasMore() {
		return nextOffset < maxOffset;
	}

	public void cancelRecentLoading() {
		if (recentCall != null) {
			recentCall.cancel();
			recentCall = null;
		}
	}
	
	private boolean recentRequestIsStillLoading() {
		return recentCall != null && recentCall.isLoading();
	}

	private void makeRequest() {
		bus.post(new OnLoadingStart());
		recentCall = new CancellableCallback<SearchResult<T>>(callback);
		doRequestHandler.call(nextOffset, LIMIT, new CallbackCallback<SearchResult<T>>(new UpdateOffsetCallback(), recentCall));
	}
	
	private class UpdateOffsetCallback implements retrofit.Callback<SearchResult<T>> {

		@Override
		public void success(SearchResult<T> t, Response response) {
			nextOffset += t.getItems().length;
			maxOffset = t.getMetaData().getTotalSize();
		}

		@Override
		public void failure(RetrofitError error) {
			
		}
		
	}
	
	public static class OnLoadingStart {}
}
