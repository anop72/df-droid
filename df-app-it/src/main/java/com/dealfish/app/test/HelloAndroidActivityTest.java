package com.dealfish.app.test;

import android.test.ActivityInstrumentationTestCase2;

import com.dealfish.app.lesson4.complete.droid.AdvancedCategoryActivity;

public class HelloAndroidActivityTest extends ActivityInstrumentationTestCase2<AdvancedCategoryActivity> {

    public HelloAndroidActivityTest() {
        super(AdvancedCategoryActivity.class); 
    }

    public void testActivity() {
    	AdvancedCategoryActivity activity = getActivity();
        assertNotNull(activity);
    }
}

